from celery import task, Celery, subtask, group
import time

REDIS_SERVER = 'redis://localhost:6379'

celery = Celery("app", broker=REDIS_SERVER, backend=REDIS_SERVER)

@task()
def task1(input):
    time.sleep(5)
    return "encode " + input

@task()
def task2(input):
    time.sleep(5)
    return "segment " + input

@task()
def task3(input):
    time.sleep(5)
    return "package  " + input

@task()
def dmap(it, callback):
    # Map a callback over an iterator and return as a group
    callback = subtask(callback)
    return group(callback.clone([arg,]) for arg in it)()