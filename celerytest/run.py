from celery import chord, chain, group
import tasks

params = ["A","B","C","D"]

task_1 = group(tasks.task1.s(p) for p in params)
task_2 = group(tasks.dmap.s(tasks.task2.s()))
task_3 = tasks.task3.s()

workflow = chord(chain([task_1, task_2]))(task_3)