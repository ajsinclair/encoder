# -*- coding: utf-8 -*-
#!/usr/bin/python
'''
Script to encode HLS and Dash from an input file and output directory
Files are placed in sub-directories call hls and dash on the specified output directory
Places encoded files in a folder of the same name as the source input

By default a set of profiles specified in the hls object is used

Usage:
./encode.py <input_file> <output_dir>

If you change the variable CMD_ONLY to True then this will not run any commands and just generate the commands

What is does
1. Analyse file (optional)
2. Encode each main rendition resolutions as an mp4 mezzanine
3. Convert the mp4 into TS segments
4. Create variant playlist
5. Fragment the mp4's for dash
6. Create the dash manifests and segments

Pre-requisites

ffmpeg compiled with libx264 and libfdk_aac
Bento tools: http://www.bok.net/trac/bento4/wiki/MpegDash

Optional: gpac / mp4box
'''

import subprocess
import sys
import os
import uuid
import traceback
import json
import requests
# from threading import Thread

# String holding the variant playlist
variant_playlist = "#EXTM3U\n"
#!/usr/bin/python
# Bitrate is in kbit/s
# -g sets GOP max distance
# -keyint_min sets GOP min distance
# -r sets frame rate at 30


ZENCODER_APIKEY=""
ZENCODER_JOBCREATE = "https://app.zencoder.com/api/v2/jobs"
ZENCODER_JOBSTATUS = "https://app.zencoder.com/api/v2/jobs/$jobid/progress.json?api_key=" + ZENCODER_APIKEY
ZENCODER_HEADERS = {"Zencoder-Api-Key":ZENCODER_APIKEY}

FFMPEG = "/usr/local/bin/ffmpeg"
FFPROBE = "/usr/local/bin/ffprobe"
#Gpax
MP4BOX = "/usr/local/bin/MP4Box"
#Bento
BENTO = "/Users/andrew/Desktop/workspace/encoder/worker/bin/osx/bento"
BENTO_EXEC = os.path.join(BENTO, "bin")
BENTO_UTILS = os.path.join(BENTO, "utils")
MP4FRAGMENT = os.path.join(BENTO_EXEC, "mp4fragment")
MP4DASH = os.path.join(BENTO_UTILS, "mp4-dash.py")

CMD_ONLY = False
PACKAGE_ONLY = False

'''
Current map for encode profiles to codecs
http://tools.ietf.org/html/rfc6381
AAC-LC
"mp4a.40.2"
HE-AAC
"mp4a.40.5"
MP3
"mp4a.40.34"
H.264 Baseline Profile level 3.0
"avc1.42001e" or "avc1.66.30"
Note: Use "avc1.66.30" for compatibility with iOS versions 3.0 to 3.1.2.
H.264 Baseline Profile level 3.1
"avc1.42001f"
H.264 Main Profile level 3.0
"avc1.4d001e" or "avc1.77.30"
Note: Use "avc1.77.30" for compatibility with iOS versions 3.0 to 3.12.
H.264 Main Profile level 3.1
"avc1.4d001f"
H.264 Main Profile level 4.0
"avc1.4d0028"
H.264 High Profile level 3.1
"avc1.64001f"
H.264 High Profile level 4.0
"avc1.640028"
H.264 High Profile level 4.1
"avc1.640028"

'''

hls = {
    "profile": {
        "type": "hls",
        "video_codec": "libx264",  #ffmpeg compatible
        "audio_codec": "libfdk_aac",  #ffmpeg compatible
        "audio_rate": 44100,  #hz
        "keyframes": 2,  # every x seconds
        "fps": 25,
        "pix_fmt": "yuv420p",
        # change this to fast or medium, this is for testing
        "preset": "medium",
        "max_rate_factor": 1.2,
        "bufsize_factor": 1,
        "fragment_duration" : 2,
        "rates":
            [
                {
                    "video_bitrate": 1800,  #kbps
                    "width": 1280,
                    "height": 720,
                    "audio_bitrate": 0,  #kbps
                    "profile": "main"
                },
                {
                    "video_bitrate": 1300,  #kbps
                    "width": 1024,
                    "height": 576,
                    "audio_bitrate": 0,  #kbps
                    "profile": "main"
                },
                {
                    "video_bitrate": 1000,  #kbps
                    "width": 960,
                    "height": 540,
                    "audio_bitrate": 0,  #kbps
                    "profile": "main"
                },
                {
                    "video_bitrate": 800,  #kbps
                    "width": 640,
                    "height": 360,
                    "audio_bitrate": 0,  #kbps
                    "profile": "main"
                },
                {
                     "video_bitrate":600, #kbps
                     "width":512,
                     "height":288,
                     "audio_bitrate":0, #kbps
                     "profile":"main"
                },
                {
                     "video_bitrate":400, #kbps
                     "width":512,
                     "height":288,
                     "audio_bitrate":0, #kbps
                     "profile":"baseline"
                },
                {
                     "video_bitrate":300, #kbps
                     "width":426,
                     "height":240,
                     "audio_bitrate":0, #kbps
                     "profile":"baseline"
                },
                {
                    "video_bitrate": 0,  #kbps
                    "width": 0,
                    "height": 0,
                    "audio_bitrate": 64,  #kbps
                    "profile": "main"
                }
            ]
    }
}
'''

,
                {
                    "video_bitrate": 1000,  #kbps
                    "width": 960,
                    "height": 540,
                    "audio_bitrate": 64,  #kbps
                    "profile": "main"
                },
                {
                    "video_bitrate": 800,  #kbps
                    "width": 640,
                    "height": 360,
                    "audio_bitrate": 64,  #kbps
                    "profile": "main"
                },
                {
                     "video_bitrate":600, #kbps
                     "width":512,
                     "height":288,
                     "audio_bitrate":64, #kbps
                     "profile":"main"
                },
                {
                     "video_bitrate":400, #kbps
                     "width":512,
                     "height":288,
                     "audio_bitrate":64, #kbps
                     "profile":"baseline"
                },
                {
                     "video_bitrate":300, #kbps
                     "width":426,
                     "height":240,
                     "audio_bitrate":64, #kbps
                     "profile":"baseline"
                }

,
                {
                    "video_bitrate": 1000,  #kbps
                    "width": 960,
                    "height": 540,
                    "audio_bitrate": 64,  #kbps
                    "profile": "main"
                },
                {
                    "video_bitrate": 800,  #kbps
                    "width": 640,
                    "height": 360,
                    "audio_bitrate": 64,  #kbps
                    "profile": "main"
                },
                {
                     "video_bitrate":600, #kbps
                     "width":512,
                     "height":288,
                     "audio_bitrate":64, #kbps
                     "profile":"main"
                },
                {
                     "video_bitrate":400, #kbps
                     "width":512,
                     "height":288,
                     "audio_bitrate":64, #kbps
                     "profile":"baseline"
                },
                {
                     "video_bitrate":300, #kbps
                     "width":426,
                     "height":240,
                     "audio_bitrate":64, #kbps
                     "profile":"baseline"
                }

{
                    "video_bitrate": 0,  #kbps
                    "width": 0,
                    "height": 0,
                    "audio_bitrate": 64,  #kbps
                    "profile": "main"
                }
,
         {
         "video_bitrate":600, #kbps
         "width":512,
         "height":288,
         "audio_bitrate":64, #kbps
         "profile":"main"
         }
'''

def zencode_files(profiles, input_file):
    url = ""
    encode_template = {
        "input": input_file,
        "region": "australia",
        "utf8":"✓",
        "output": [
            {
                "url": url,
                "width": 1920,
                "video_bitrate": 2000,
                "h264_profile": "high",
                "decoder_bitrate_cap": 2000,
                "decoder_buffer_size": 4000,
                "h264_level": "4.1",
                "audio_bitrate": 54,
                "audio_channels": 2,
                "forced_keyframe_rate": ".5",
                "public": 1
            }
        ]
    }

    # Submit the encode job to Zencoder
    output = json.dumps(encode_template)
    #print output
    resp = requests.post(ZENCODER_JOBCREATE, data=output, headers=ZENCODER_HEADERS)
    print resp.text

    job = json.loads(resp.text)
    encode_complete = False
    job_id = job["id"]
    notdone = True


def encode_file(infile, width, height, bitrate, audio_bitrate, vprofile="main", output_dir=".", profile=hls,
                video_only=False, audio_only=False, fragment_duration=2):
    file_no_ext = os.path.splitext(os.path.basename(file))[0]
    # Creates a new file in the format filename_widthxheight.mp4

    if not os.path.exists(output_dir):
        os.makedirs(output_dir)

    new_file = os.path.join(output_dir, ("%(filenameNoExt)s_%(bitrate)s_%(width)sx%(height)s.mp4"
                % {"bitrate": bitrate, "height": height, "width": width, "filenameNoExt": file_no_ext}))
    g = int(hls["profile"]["fps"]) * 4
    maxrate = int(bitrate * hls["profile"]["max_rate_factor"])
    bufsize = int(bitrate * hls["profile"]["bufsize_factor"])
    output_file = os.path.join(output_dir, new_file)
    # -movflags frag_keyframe+empty_moov
    if video_only:
        audio_params = "-an"
    else:
        audio_params = "-c:a %(audio_codec)s -ar %(audio_rate)s -b:a %(audio_bitrate)sk" % ({
            "audio_codec": hls["profile"]["audio_codec"],
            "audio_rate": hls["profile"]["audio_rate"],
            "audio_bitrate": audio_bitrate
        })

    if audio_only:
        video_params = "-vn"
        new_file = os.path.join(output_dir, ("%(filenameNoExt)s_%(bitrate)s_audio.m4a"
                % {"bitrate": audio_bitrate, "filenameNoExt": file_no_ext}))
        output_file = os.path.join(output_dir, new_file)
    else:
        video_params = "-c:v %(video_codec)s -r %(fps)s -g %(g)s -b:v %(bitrate)sk -force_key_frames 'expr:gte(t,n_forced*%(fragment_duration)s)' "\
                       "-maxrate %(maxrate)sk -bufsize %(bufsize)sk -s %(width)sx%(height)s -profile:v %(profile)s " \
                       " -pix_fmt %(pix_fmt)s -preset %(preset)s" % ({
                    "bitrate" : bitrate,
                    "height" : height,
                    "width" : width,
                    "video_codec" : hls["profile"]["video_codec"],
                    "fps" : hls["profile"]["fps"],
                    "g" : g,
                    "pix_fmt" : hls["profile"]["pix_fmt"],
                    "profile" : vprofile,
                    "preset" : hls["profile"]["preset"],
                    "maxrate" : maxrate,
                    "bufsize" : bufsize,
                    "fragment_duration" : fragment_duration
        })

    ffmpegcmd = ("%(ffmpeg)s -y -i %(filename)s  " \
                 " "
                 "%(video_params)s " \
                 # The next line can be uncommented for doing test runs on 30 second files
                 #" -ss 00:00:00 -t 30 " \
                 "%(audio_params)s  %(newFile)s" \
                 % {
                    "ffmpeg" : FFMPEG,
                    "filename" : infile,
                    "video_params" : video_params,
                    "audio_params" : audio_params,
                    "newFile" : output_file,

                })

    print ffmpegcmd
    if not CMD_ONLY and not PACKAGE_ONLY:
        process = subprocess.Popen(ffmpegcmd, shell=True, stdout=subprocess.PIPE, stderr=subprocess.PIPE)
        print process.communicate()
    return output_file


def encode_file_hls(file, bitrate, outputdir, audio_only=False):
    fileNoExt = os.path.splitext(os.path.basename(file))[0]
    outputdir = os.path.join(outputdir,"hls")
    if not os.path.exists(outputdir):
        os.makedirs(outputdir)


    ffmpegcmd = ("%(ffmpeg)s -y -i %(filename)s -codec copy -map 0 -f segment -segment_list index_%(bitrate)s" \
                 ".m3u8 -segment_time 10 -bsf h264_mp4toannexb -segment_list_type m3u8 %(filenameNoExt)s_%(count)s.ts" \
                 % {
        "filename": file,
        "filenameNoExt": fileNoExt,
        "bitrate": bitrate,
        "count": "%03d",
        "ffmpeg": FFMPEG
    })
    print ffmpegcmd
    append_variant_playlist(bitrate, audio_only)
    if not CMD_ONLY:
        process = subprocess.Popen(ffmpegcmd, shell=True, stdout=subprocess.PIPE, cwd=outputdir)
        print process.communicate()

    return


def generate_thumbnail(video_file, target_dir, seconds_from_start):
    # TODO add -s WxH for correct aspect


    out_file = os.path.splitext(os.path.basename(video_file))[0] + ".jpg"
    ffmpeg_cmd = ("%(ffmpeg_cmd)s -y -i %(video_file)s -ss %(time)s -f image2 -vframes 1 %(out_file)s" % \
                  {"ffmpeg_cmd":FFMPEG, "video_file":video_file, "out_file":out_file, "time":seconds_from_start})

    process = subprocess.Popen(ffmpeg_cmd, shell=True, stdout=subprocess.PIPE, stderr=subprocess.PIPE \
                               , cwd=target_dir)
    so, se = process.communicate()
    return out_file

def append_variant_playlist(bitrate, audio_only):
    '''
    Appends each individual encode to the playlist
    Best reference for sample is here: https://gist.github.com/sinkers/9d8d78f3aa677eec4520
    '''
    global variant_playlist
    extBitrate = int(bitrate * 1000 * hls["profile"]["max_rate_factor"])
    # TODO tidy up so that it supports both basic a/v muxed into the TS and also seperate
    if audio_only:
        #variant_playlist += ("#EXT-X-STREAM-INF:PROGRAM-ID=1,BANDWIDTH=%(bitrate)s\n" % {"bitrate": extBitrate})
        # Currently this is a bit of a hack and will only work when the seperate audio encoding is used
        variant_playlist += ('#EXT-X-MEDIA:TYPE=AUDIO,GROUP-ID="audio",LANGUAGE="en",NAME="English",AUTOSELECT=YES,DEFAULT=YES,URI="index_%(bitrate)s.m3u8"\n' % {"bitrate": bitrate})
    else:
        # TODO add codec map
        variant_playlist += ('#EXT-X-STREAM-INF:PROGRAM-ID=1,BANDWIDTH=%(bitrate)s,CODECS="avc1.4d001f,mp4a.40.2",AUDIO="audio"\n' % {"bitrate": extBitrate})
        variant_playlist += ("index_%(bitrate)s.m3u8\n" % {"bitrate": bitrate})
    return


def create_variant_playlist(output_dir="."):
    print variant_playlist
    if not CMD_ONLY:
        indexfile = os.path.join(output_dir,"hls","index.m3u8")
        f = open(indexfile, "w")
        f.write(variant_playlist)
        f.close()
    return

def get_encrypt_key(assetid=None):
    # This is where we would do lookups or create keys etc
    encrypt_settings = {
        "keyid": "f0deb27d50e34094aa2704c5166c43da",
        "key" : "#XVBovsmzhP9gRIZxWfFta3VVRPzVEWmJsazEJ46I",
        "laurl" : "http://playready.directtaps.net/pr/svc/rightsmanager.asmx"
    }
    return encrypt_settings

def get_encrypt_key_tp(assetid=None):
    '''
    Content Key: 0f80c912-a80d-4c97-9d50-f16c34ac0b4d
    Key ID: 228d664438d698ce6fbe0608a94d23d0
    Server: https://playready.entitlement.next.theplatform.com/playready/rightsmanager.asmx

    Content key decoded using
    uuid.UUID("099c581b-091a-460d-b81c-96558c1b9106").hex
    099c581b091a460db81c96558c1b9106

    '''
    encrypt_settings = {
        "key": "099c581b091a460db81c96558c1b9106",
        "keyid" : "228d664438d698ce6fbe0608a94d23d0",
        "laurl" : "https://playready.entitlement.next.theplatform.com/playready/rightsmanager.asmx"
    }
    return encrypt_settings

def create_dash(dash_files, output_dir=".", fragment_duration=2000, encrypt=False, pro=False, ivcompat=False,
                nosplit=False, segmenttimeline=False, smooth=False, segmentlist=False):
    dashed_files = []
    # TODO this has become quite inefficient if generating multiple packages as the files only need to be fragmented once
    for item in dash_files:
        file_no_ext = os.path.splitext(item)[0]
        dashed_file = file_no_ext + "_dash.mp4"
        if not os.path.exists(output_dir):
            os.makedirs(output_dir)

        outfile = os.path.join(output_dir, dashed_file)
        fragcmd = "%(mp4fragment)s --fragment-duration %(fragment_duration)s %(input)s %(dashed_file)s" % ({
                                                                     "mp4fragment": MP4FRAGMENT,
                                                                     "input": item,
                                                                     "dashed_file": outfile,
                                                                     "fragment_duration" : fragment_duration
                                                                 })
        print fragcmd
        if not CMD_ONLY:
            process = subprocess.Popen(fragcmd, shell=True, stdout=subprocess.PIPE)
            print process.communicate()
        dashed_files.append(dashed_file)
        output = os.path.join(output_dir, "dash")

    if encrypt:
        encrypt_settings = get_encrypt_key_tp()
        keyid = encrypt_settings["keyid"]
        key = encrypt_settings["key"]
        laurl = encrypt_settings["laurl"]

        encrypt_command = "--encryption-key=%(keyid)s:%(key)s" % ({
            "keyid" : keyid,
            "key" : key
        })
    else:
        encrypt_command = ""

    if pro:
        pro_command = " --playready-add-pssh"
        pro_command += " --playready-header=LA_URL:%(laurl)s#KID:%(keyid)s" % ({
            "keyid" : keyid,
            "laurl" : laurl
        })
        if ivcompat:
            pro_command += ' --encryption-args="--global-option mpeg-cenc.piff-compatible:true"'
            output = os.path.join(output_dir, "dash-ivcompat")
    else:
        pro_command = ""
    '''
    DASH android for Playready SDK

    '''
    extracmds = ""
    if nosplit:
        extracmds += " --no-split --use-segment-list"
        output = output + "-nosplit"

    if segmenttimeline:
        extracmds += " --use-segment-timeline"

    if segmentlist:
        extracmds += " --use-segment-list"

    if smooth:
        extracmds += " --smooth"
        # For testing let's put this in a dedicated dir
        output = os.path.join(output_dir, "ism")

    dashcmd = "%(mp4dash)s --verbose -f %(dash_files)s -o %(output)s -m manifest.mpd %(encrypt)s"\
              "%(pro)s --exec-dir %(exec_dir)s %(extracmds)s" % \
              ({
                   "output": output,
                   "mp4dash": MP4DASH,
                   "dash_files": " ".join(dashed_files),
                   "exec_dir": BENTO_EXEC,
                   "encrypt": encrypt_command,
                   "pro": pro_command,
                   "extracmds" : extracmds
               })
    print dashcmd
    if not CMD_ONLY:
        process = subprocess.Popen(dashcmd, shell=True, stdout=subprocess.PIPE, stderr=subprocess.PIPE)
        print process.communicate()
    return os.path.join(output, "manifest.mpd")


def create_dash_gpac(dash_files, output_dir="./dash", manifest="manifest.mpd"):
    output = os.path.join(output_dir, manifest)
    gpaccmd = "%(mp4box)s -profile main -dash 2000 -frag 6000 -segment-timeline -out %(manifest)s %(dash_files)s" % \
              ({
                   "mp4box": MP4BOX,
                   "dash_files": " ".join(dash_files),
                   "manifest": manifest
               })
    print gpaccmd
    process = subprocess.Popen(gpaccmd, shell=True, stdout=subprocess.PIPE)
    print process.communicate()


def create_smooth(dash_files, output_dir="./ism", manifest="manifest.mpd"):
    return


def create_dir(file):
    return


dash_files = []
try:
    file = sys.argv[1]
    if not os.path.exists(file):
        print "Input file not valid"
        raise Exception

    output_dir = sys.argv[2]
    if output_dir is None:
        os.makedirs(output_dir)
        print "No output dir specified, creating"
        #output_dir = os.path.abspath(os.path.dirname(__file__))
    PACKAGE_ONLY = True

    for item in hls["profile"]["rates"]:
        # Encode seperate audio and video tracks by specifying encode profiles with no resolution or no audio rate
        if item["width"] > 0 and item["height"] > 0 and item["audio_bitrate"] > 0:
            encoded_file = encode_file(file, item["width"], item["height"], item["video_bitrate"], item["audio_bitrate"],
                fragment_duration=hls["profile"]["fragment_duration"], output_dir=output_dir, video_only=False, audio_only=False)
            encode_file_hls(encoded_file, item["video_bitrate"], output_dir)
        # Audio only
        elif item["width"] <= 0 and item["height"] <= 0 and item["audio_bitrate"] > 0:
            encoded_file = encode_file(file, item["width"], item["height"], item["video_bitrate"], item["audio_bitrate"],
                output_dir=output_dir, video_only=False, audio_only=True)
            encode_file_hls(encoded_file, item["audio_bitrate"], output_dir, audio_only=True)
        elif item["width"] > 0 and item["height"] > 0 and item["audio_bitrate"] <= 0:
            encoded_file = encode_file(file, item["width"], item["height"], item["video_bitrate"], item["audio_bitrate"],
                fragment_duration=hls["profile"]["fragment_duration"], output_dir=output_dir, video_only=True, audio_only=False)
            encode_file_hls(encoded_file, item["video_bitrate"], output_dir)
        print encoded_file
        dash_files.append(encoded_file)


    create_variant_playlist(output_dir=output_dir)
    #createDASHGpac(dash_files)
    # Works for Chromecast
    #create_dash(dash_files, output_dir=output_dir, encrypt=True, pro=True, smooth=False,
    #                  fragment_duration=(int(hls["profile"]["fragment_duration"]) * 1000), ivcompat=True, nosplit=False)
    # Works for Android
    #create_dash(dash_files, output_dir=output_dir, encrypt=True, pro=True, smooth=False,
    #                  fragment_duration=(int(hls["profile"]["fragment_duration"]) * 1000), ivcompat=True, nosplit=True)
    #Create a smooth version
    create_dash(dash_files, output_dir=output_dir, encrypt=True, pro=True, smooth=False, segmentlist=False, segmenttimeline=False,
                      fragment_duration=(int(hls["profile"]["fragment_duration"]) * 1000), ivcompat=True, nosplit=False)
    # Create a HbbTV version
    '''
    Needs MS playready urn
    Multiple Hbb live profile
    Needs scheme id = 7
    Has to have PRO
    '''
except:
    print "Usage: encode.py <input_file> <output_dir>"
    traceback.print_exc()






