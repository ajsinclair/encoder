import requests
import json

# Test adding working
print "Adding workers"
r = requests.get("http://localhost:19000/worker/register?address=http://localhost:19001")
print r.text
r = requests.get("http://localhost:19000/worker/register?address=http://localhost:19002")
print r.text
r = requests.get("http://localhost:19000/worker/register?address=http://localhost:19003")
print r.text
worker = json.loads(r.text)

print "Worker list"
r = requests.get("http://localhost:19000/workers")
for w in json.loads(r.text):
    print w


print "Remove worker" + worker["id"]
req = "http://localhost:19000/worker/remove?id=" + worker["id"]
print req
r = requests.get(req)
print r.text

print "Worker list"
r = requests.get("http://localhost:19000/workers")
for w in json.loads(r.text):
    print w