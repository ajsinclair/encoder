hls = {
           "profile": {
                       "type":"hls",
                       "video_codec":"libx264", #ffmpeg compatible
                       "audio_codec":"libfdk_aac", #ffmpeg compatible
                       "audio_rate":44100, #hz
                       "keyframes":2, # every x seconds
                       "fps":25,
                       "pix_fmt": "yuv420p",
                       "preset":"medium",
                       "rates": 
                       [
                          {
                          "video_bitrate":2500, #kbps
                          "size": "1280x720",
                          "audio_bitrate":128, #kbps
                          "profile":"main"
                          },
                          {
                          "video_bitrate":1800, #kbps
                          "size": "1024x576",
                          "audio_bitrate":64, #kbps
                          "profile":"main"
                          },
                          {
                          "video_bitrate":1200, #kbps
                          "size": "960x540",
                          "audio_bitrate":64, #kbps
                          "profile":"main"
                          },
                          {
                          "video_bitrate":1000, #kbps
                          "size": "1280x720",
                          "audio_bitrate":64, #kbps
                          "profile":"main"
                          }
              
                      ]
                    }
           }