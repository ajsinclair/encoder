'''
Master job management process

Accepts new job objects and if required it then splits the job into multiple tasks
and distributes the tasks to the worker nodes

Startup:



Can be scaled for different encoding farm configurations i.e.
big / high cpu nodes may be more efficient with tasks per worker
'''

from flask import Flask, request
import json
import os
import subprocess
from Queue import Queue
from celery import Celery, chain

#import our classes
from worker import Worker

import splitstitch

app = Flask(__name__)


app.config.update(
    CELERY_BROKER_URL='redis://localhost:6379',
    CELERY_RESULT_BACKEND='redis://localhost:6379'
)

celery = Celery(app, broker='redis://localhost:6379')

# This is the URL that this server listens on
MASTER_URL = "http://localhost:19000/"

#This is a list of workers
workers = []

# List of jobs that need to be processed
jobs = Queue()


job = {
     # Unique ID of this job
     "jobid":None,
     # URI e.g. s3:// ftp:// file://
     "input":None,
     # URI e.g. s3:// ftp:// file://
     "destination":None,
     "destination_credentials":
        {
         # Example only could also be S3 access key etc
         "user":None,
         "pass":None
         },
     "callback":MASTER_URL,
     # This is the spec for a job that needs to be done which is likely a single transcode
     "profile":None
     }


@app.route('/worker/register', methods=['GET','POST'])
def worker_register():
    '''
    Adds a worker to the workers list
    Takes a URL in as the workers address e.g. /worker/register?address=http://192.168.1.5:19003
    '''
    address = request.args.get('address')
    w = Worker(address)
    workers.append(w)
    response = {"id":w.get_id()}
    return json.dumps(response)

@app.route('/worker/remove', methods=['GET','POST'])
def worker_remove():
    id = request.args.get('id')
    for worker in workers:
        if worker.get_id() == id:
            workers.remove(worker)
            return "Removed %s" % id
    return "Not found"

@app.route('/workers')
def get_workers():
    response = []
    for worker in workers:
        response.append({
                         "id":worker.get_id(),
                         "address":worker.get_address()
                         })
    return json.dumps(response)

@app.route('/job', methods=['GET'])
def create_job():
    '''
    Takes a JSON job object and creates a series of encode tasks for each specified output
    To do a split and stitch encode will need to happen in a specific order, that is some jobs will need specific predecessors
    
    1. Encode to mezz format in chunks, this will create a series of sub-tasks
    2. Stitch together
    3. Package into target formats
    '''
    #job = [["ls /tmp","ls /tmp","ls /tmp"],"ls /Users/andrew/Desktop","ls /Users/andrew/Desktop/workspace"]
    #job = ["ls"]
    chain(
            splitstitch.encode(None),         # Fetch data from remote source
            splitstitch.stitch(None),                # Remove blacklisted records
            splitstitch.package(None)               # Transform raw data ready for loadin                  # Load into DB
        ).apply_async()
    return "OK"

@app.route('/task/next')
def get_next_task():
    '''
    Returns the next task to any requesting slave from the current task queue
    '''
    pass

@celery.task(name="master.runjob")
def runjob(cmd):
    #r = envoy.run(cmd)
    #logging.error(r.std_out)
    #logging.error(r.std_err)
    process = subprocess.Popen(cmd,shell=True, stdout=subprocess.PIPE, stderr=subprocess.PIPE)
    r = process.communicate()
    requests.post("http://localhost:50000/status",data={
                                                        "status":"done",
                                                        "out":r,
                                                        "err":r,
                                                        "cmd":cmd,
                                                        "returncode": process.returncode
                                                        })
    return "OK"

@celery.task
def create_task(job, output):
    '''
    Takes an output and creates an encode task
    '''
    runjob(job)
    
    return "task"



if __name__ == '__main__':
    app.run(host='0.0.0.0',port=60000,debug=True)