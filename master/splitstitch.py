import shutil
import os
import time
import subprocess

from celery import task

@task()
def encode(source):
    time.sleep(5)
    return "OK"

@task()
def stitch(source):
    time.sleep(5)
    return "True"

@task()
def package(source):
    time.sleep(5)
    return "True"