import uuid

class Worker:
    # http network address of the worker
    address = ""
    # If the worker as an active task
    working = False
    # number of tasks this worker will take
    task_max = 1
    # current number of tasks assigned
    task_count = 0
    # The last 5 min load average of the worker
    last_load = 0
    # Number of cores on the worker
    cores = 1
    # id of the worker
    id = None

    def __init__(self, address):
        self.address = address
        self.id = uuid.uuid4().hex
    
    def get_id(self):
        return self.id
    
    def get_address(self):
        return self.address
    
    def set_working(self):
        self.working = True
        
    def set_idle(self):
        self.working = False
        
    def inc_taskcount(self):
        if self.task_count > 0:
            self.task_count += 1
            
    def get_taskcount(self):
        return self.task_count
            
    def set_task_max(self, task_max):
        self.task_max = task_max
        
    def get_task_max(self):
        return self.task_max
    
     	