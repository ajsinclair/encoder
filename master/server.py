from tornado.wsgi import WSGIContainer
from tornado.httpserver import HTTPServer
from tornado.ioloop import IOLoop
from master import app

STARTUP_PORT = 19000

http_server = HTTPServer(WSGIContainer(app))
http_server.bind(STARTUP_PORT)
http_server.start(0)
IOLoop.instance().start()

