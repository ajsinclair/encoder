'''
Simple program to take a JSON object of encoding commands and pipe these to ffmpeg
Progress is tracker back via the worker that started the encode

'''
import subprocess

FFMPEG = "/usr/local/bin/ffmpeg"
           
def encode_hls(input_file, encode_profile):
    '''
    Encodes to HLS format
    '''
    hls_template = ("%(ffmpeg)s -i %(input_file)s -c:v libx264 -b:v %(video_bitrate) " %
                    {
                     "ffmpeg":FFMPEG,
                     "input": input_file,
                     "keyframe_interval": encode_profile["keyframes"],
                     "video_bitrate": encode_profile["video_bitrate"]
                     
                     
                     })
    p = subprocess.Popen()
    pass