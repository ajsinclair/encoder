'''
Polls the master for new encode jobs and excutes them

This apps celery worker is started as follows
celery -A worker:celery worker -l info

'''
import time
import requests
from flask import Flask, request
import logging
import socket
import subprocess
import json
import logging
import traceback
import sys

# import our tasks list
import tasks

from celery import Celery

app = Flask(__name__, static_url_path='/files', static_folder='tmp')
app.config.update(
    CELERY_BROKER_URL='redis://localhost:6379',
    CELERY_RESULT_BACKEND='redis://localhost:6379'
)

platform = sys.platform
if platform.startswith('linux'):
    platform = 'linux-x86'
elif platform.startswith('darwin'):
    platform = 'macosx'

celery = Celery(app, broker='redis://localhost:6379')

MASTER = "http://localhost:19000"
SLEEP_TIME = 60

def register(port):
    try:
        server = socket.gethostbyname(socket.gethostname())
        resp = requests.get(MASTER + "/worker/register?address=http://" + server + ":" + str(port))
        return resp.text
    except:
        return "Unable to connect to master"

def get_job():
    resp = requests.get(MASTER + "/task/next")
    pass

@app.route('/mysocket')
def get_socket():
    return server.socket

@celery.task(name="worker.add")
def add(x, y):
    logging.error(x)
    r = envoy.run(x)  
    return r.std_out

@celery.task(name="worker.runjob")
def runjob(cmd):
    #r = envoy.run(cmd)
    #logging.error(r.std_out)
    #logging.error(r.std_err)
    process = subprocess.Popen(cmd,shell=True, stdout=subprocess.PIPE, stderr=subprocess.PIPE)
    r = process.communicate()
    requests.post("http://localhost:50000/status",data={
                                                        "status":"done",
                                                        "out":r,
                                                        "err":r,
                                                        "cmd":cmd,
                                                        "returncode": process.returncode
                                                        })
    return "OK" 

@app.route('/job', methods=['POST','GET'])
def do_job():
    try:
        job = json.loads(request.data)
        logging.error(request.data)
        
        source_file = "http://content6.video.news.com.au/xnczlwbzpBzZzf3Ouyt5mJ8IpvUrNuxB/DOcJ-FxaFrRg4gtDEwOjEzYzowbTu7_H"
        splitgroup = group(tasks.split.s(source_file,"00:00:00.000","30","1"),
              tasks.split.s(source_file,"00:00:30.000","30","2"),
              tasks.split.s(source_file,"00:01:00.000","30","3"))

        callback = tasks.stitch2.s()
        mychord = chord(splitgroup)(callback)
        #cmd = job['cmd']
        #res = runjob.apply_async(([cmd]))
        
        return "OK"
    except:
        logging.error(traceback.print_exc())
        return "error"

''' 
Currently issue with wsgi is processing chunked transfer encoding that ffmpeg sends

'''
@app.route('/status', methods=['POST','GET'])
def get_status():
    logging.error(request.headers)
    logging.error(request.form)
    logging.error(request.args)
    return "OK"

def run():
    while True:
        time.sleep(SLEEP_TIME)
        pass
    
if __name__ == '__main__':
    app.run(host='0.0.0.0',port=50000,debug=True) 