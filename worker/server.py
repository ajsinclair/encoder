from tornado.wsgi import WSGIContainer
from tornado.httpserver import HTTPServer
from tornado.ioloop import IOLoop
import tornado.netutil
import requests
import os
import netifaces

from worker import app
from worker import register

NETIF = "en0"
socket = 0
ip = 0
CURRENT_DIR = os.path.abspath(os.path.dirname(__file__))

sockets = tornado.netutil.bind_sockets(0, '')
server = tornado.httpserver.HTTPServer(WSGIContainer(app))
server.add_sockets(sockets)

for s in sockets:
    print('Listening on %s, port %d' % s.getsockname()[:2])
    ip, socket = s.getsockname()[:2]

serverip = netifaces.ifaddresses(NETIF)[2][0]['addr']

print ip, socket
port_file = os.path.join(CURRENT_DIR,"port")
f = open(port_file,'w')
f.write('{"socket":"%s", "ip":"%s"}' % (socket, serverip))
f.close()
#print register(socket)

tornado.ioloop.IOLoop.instance().start()

#TODO need to be able to return this socket to another app

'''
http_server = HTTPServer(WSGIContainer(app))
http_server.bind(0)
http_server.start(0)
IOLoop.instance().start()
'''

