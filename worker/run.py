from celery import chain, Celery, group, chord
import tasks
import json
 
celery = Celery("app", broker='redis://localhost:6379',backend='redis://localhost:6379')

source_file = "http://content6.video.news.com.au/xnczlwbzpBzZzf3Ouyt5mJ8IpvUrNuxB/DOcJ-FxaFrRg4gtDEwOjEzYzowbTu7_H"
#source_file = "/Users/andrew/Desktop/demo/67420370.mp4"

params = [
          {
            "input":source_file,
            "bitrate":600,
            "height":288,
            "width":512,
            "video_codec":"libx264",
            "audio_codec":"libvo_aacenc",
            "audio_rate":44100,
            "fps":25,
            "pix_fmt":"yuv420p",
            "vprofile":"main",
            "preset":"fast",
            "max_rate_factor":1.2,
            "bufsize_factor":1.2,
            "keyframe_int": 6
            },
          {
            "input":source_file,
            "bitrate":700,
            "height":360,
            "width":640,
            "video_codec":"libx264",
            "audio_codec":"libvo_aacenc",
            "audio_rate":44100,
            "fps":25,
            "pix_fmt":"yuv420p",
            "vprofile":"main",
            "preset":"fast",
            "max_rate_factor":1.2,
            "bufsize_factor":1.2,
            "keyframe_int": 6
            }

          ]


''',
          {
            "input":source_file,
            "bitrate":1000,
            "height":540,
            "width":960,
            "video_codec":"libx264",
            "audio_codec":"libvo_aacenc",
            "audio_rate":44100,
            "fps":25,
            "pix_fmt":"yuv420p",
            "vprofile":"main",
            "preset":"fast",
            "max_rate_factor":1.2,
            "bufsize_factor":1.2,
            "keyframe_int": 6
            },
          {
            "input":source_file,
            "bitrate":1200,
            "height":576,
            "width":1024,
            "video_codec":"libx264",
            "audio_codec":"libvo_aacenc",
            "audio_rate":44100,
            "fps":25,
            "pix_fmt":"yuv420p",
            "vprofile":"main",
            "preset":"fast",
            "max_rate_factor":1.2,
            "bufsize_factor":1.2,
            "keyframe_int": 6
            }
            '''


'''
Do a normal encode and the package
1. Encode to TS
2. Package as HLS (each package can occur as soon as any item in step 1 is done)
3. Generate index files - needs all items in step 2 to have finished
'''
#package = group(tasks.package_hls.s(p) for p in params)

'''
# The chord approach
encode = (tasks.encode.s(p) for p in params)
package_hls = (tasks.dmap.s(tasks.package_hls.s()))
fragment_dash = (tasks.dmap.s(tasks.fragment_dash.s()))
create_hls_manifest = tasks.create_hls_manifest.s()

workflow = chord([encode, package_hls, fragment_dash])(create_hls_manifest)
print workflow.get()
'''

# Works but calls create manifest out of sync and relies on evil hack where task waits for status to be ready
encodegroup = (
               group(tasks.encode.s(p) for p in params) |
               tasks.dmap.s(tasks.package_hls.s()) | tasks.dmap.s(tasks.fragment_dash.s())
              )

encodechain = chain(encodegroup | tasks.chordfinisher.s() | tasks.create_hls_manifest.s())
result = encodechain.apply_async()
#result = chord(header=encodegroup, body=tasks.create_hls_manifest.s()).delay()
#result = encodegroup.apply_async(link=tasks.dmap.s(tasks.create_hls_manifest.s()))

# Get waits for all the results
print result.get()

if result.ready():
    for item in result.result:
        print item.result["index_file"]



'''
# These tests run a split on the encodes
splitgroup = group(tasks.split.s(source_file,"00:00:00.000","30","1"),
              tasks.split.s(source_file,"00:00:30.000","30","2"),
              tasks.split.s(source_file,"00:01:00.000","10","3"))

callback = tasks.stitch2.s()
mychord = chord(splitgroup)(callback)

result = mychord.get()
print json.dumps(result, indent=4)
'''

'''
split = splitgroup.apply_async()
split.link(tasks.stitch.s())
print split.ready()
print split.successful()
output = split.join()
print split.children
for item in output:
    print item["output_file"]
'''
chain(
    #tasks.stitch.s(),
    #tasks.runcmd.s(cmd=cmd)
).apply_async()
