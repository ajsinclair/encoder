import shutil
import os
import subprocess
import os
import json
from urlparse import urlparse
from celery import task, Celery, subtask, group
import time
from celery.result import GroupResult, AsyncResult
'''
Run this to start: celery -A tasks:celery worker -l info -n <name>
use -concurrency <num> to set how man concurrent tasks on this host
'''
 
CURRENT_DIR = os.path.abspath(os.path.dirname(__file__))
REDIS_SERVER = 'redis://localhost:6379'

celery = Celery("app", broker=REDIS_SERVER, backend=REDIS_SERVER)

OUTPUT_DIR = os.path.join(CURRENT_DIR, "tmp") 
# os.path.join(CURRENT_DIR,"bin/osx/ffmpeg")
# Note that this ffmpeg doesn't have libfdk_aac so need to build a static version at some point
# Path needs to be set for the OS
OS = "osx"
FFMPEG = os.path.join(CURRENT_DIR, "bin/" + OS + "/ffmpeg")
FFLOGLEVEL = "warning"
BENTO_EXEC = os.path.join(CURRENT_DIR, "bin", OS, "bin")
BENTO_UTILS = os.path.join(CURRENT_DIR, "bin", OS, "utils")
MP4FRAGMENT = os.path.join(BENTO_EXEC, "mp4fragment")
MP4DASH = os.path.join(BENTO_UTILS, "mp4-dash.py")

def get_serverdetails():
    port_file = os.path.join(CURRENT_DIR, "port")
    f = open(port_file, 'r')
    server_details = json.loads(f.read())
    f.close()
    return server_details

def run_ffmpeg_cmd(cmd):
    process = subprocess.Popen(cmd,shell=True, stdout=subprocess.PIPE, stderr=subprocess.PIPE)
    so, se = process.communicate()
    if process.returncode != 0:
        raise Exception
    else:
        return {
                "out" : so,
                "err" : se,
                "cmd" : cmd,
                "returncode" : process.returncode,
                }
 
    
@task()
def encode(params, dest=None):
    '''
    Encodes a file with a set of parameters
    If dest is None it just writes to the local temp directory and returns a local URL assuming final result is further packaging

    '''
    task_id = encode.request.id
    server_details = get_serverdetails()
    # This is the path the local file
    output_file = os.path.join(OUTPUT_DIR, "%s.ts" % (task_id))
    # This is the URL of the output file on this worker server
    output_url = "http://%(server)s:%(port)s/files/%(task_id)s.ts" % ({
                                                                         "server": server_details["ip"],
                                                                         "port": server_details["socket"],
                                                                         "task_id": task_id
                                                                         })
    g = int(params["fps"]) * 4
    maxrate = int(params["bitrate"] * params["max_rate_factor"])
    bufsize = int(params["bitrate"] * params["bufsize_factor"])
    
    cmd= ("%(ffmpeg)s -y -i %(filename)s -c:v %(video_codec)s -r %(fps)s -g %(g)s " \
        "-b:v %(bitrate)sk -force_key_frames 'expr:gte(t,n_forced*%(keyframe_int)s)' -maxrate %(maxrate)sk "
        " -bufsize %(bufsize)sk -s %(width)sx%(height)s -profile:v %(profile)s " \
        # For testing only
        " -ss 00:00:00 -t 5 " \
        "-c:a %(audio_codec)s -ar %(audio_rate)s -b:a %(audio_rate)sk -preset %(preset)s %(newFile)s" \
        % {
        "ffmpeg": FFMPEG,
        "filename": params["input"],
        "bitrate": params["bitrate"],
        "height": params["height"],
        "width": params["width"],
        "newFile": output_file,
        "video_codec": params["video_codec"],
        "audio_codec": params["audio_codec"],
        "audio_rate": params["audio_rate"],
        "fps": params["fps"],
        "g": g,
        "pix_fmt": params["pix_fmt"],
        "profile": params["vprofile"],
        "preset": params["preset"],
        "maxrate": maxrate,
        "bufsize": bufsize,
        "keyframe_int": params["keyframe_int"]
        })
    process = subprocess.Popen(cmd,shell=True, stdout=subprocess.PIPE, stderr=subprocess.PIPE)
    so, se = process.communicate()
    if process.returncode != 0:
        raise Exception
    return {
                "status":"done",
                "out":so,
                "err":se,
                "cmd":cmd,
                "returncode": process.returncode,
                "source_file": params["input"],
                "output_file": output_file,
                "output_url": output_url,
                "params":params
                }
    pass

@task()
def split(source_file, start, finish, part):
    """
    This command does a split encode using a start and end time and encodes to a MPEG-TS so the output
    can be concat'd together
    """
    task_id = split.request.id
    server_details = get_serverdetails()
    output_file = os.path.join(OUTPUT_DIR, "%s_part_%s.ts" % (task_id, part))
    output_url = "http://%(server)s:%(port)s/files/%(task_id)s_part_%(part)s.ts" % ({
                                                                         "server":server_details["ip"],
                                                                         "port":server_details["socket"],
                                                                         "part":part,
                                                                         "task_id":task_id
                                                                         })
    cmd = "%(ffmpeg)s -y -loglevel %(ffloglevel)s -i %(source_file)s -ss "\
    "%(start)s -t %(finish)s -force_key_frames 'expr:gte(t,n_forced*2)' -c:v libx264 -s 640x360 -b:v 1000k %(output_file)s" %\
    ({
      "start":start,
      "finish":finish,
      "part":part,
      "source_file":source_file,
      "output_file":output_file,
      "ffmpeg":FFMPEG,
      "ffloglevel":FFLOGLEVEL
      })
    process = subprocess.Popen(cmd,shell=True, stdout=subprocess.PIPE, stderr=subprocess.PIPE)
    so, se = process.communicate()
    if process.returncode != 0:
        raise Exception
    return {
                "status":"done",
                "out":so,
                "err":se,
                "cmd":cmd,
                "returncode": process.returncode,
                "source_file": source_file,
                "output_file": output_file,
                "output_url": output_url
                }

@task()
def fragment_dash(input):
    # This function would be to individually perform a fragment similar to HLS
    logger = fragment_dash.get_logger()
    logger.info(input)
    return {
                "task": "fragment_dash",
                "status":"done",
                "out":"so",
                "err":"so,",
                "cmd":"cmd",
                "returncode": 0,
                "source_file": "output_file",
                "output_file": "output_file",
                "index_file": "index_file"
                }

@task()
def package_dash(input, output_dir="."):
    dashed_files = []
    for item in input:
        file_no_ext = os.path.splitext(item)[0]
        dashed_file = file_no_ext + "_dash.mp4"
        if not os.path.exists(output_dir):
            os.makedirs(output_dir)

        outfile = os.path.join(output_dir, dashed_file)
        fragcmd = "%(mp4fragment)s %(input)s %(dashed_file)s" % ({
                                                                     "mp4fragment": MP4FRAGMENT,
                                                                     "input": item,
                                                                     "dashed_file": outfile
                                                                 })
        print fragcmd
        process = subprocess.Popen(fragcmd, shell=True, stdout=subprocess.PIPE)
        print process.communicate()
        dashed_files.append(dashed_file)
        output = os.path.join(output_dir, "dash")

    dashcmd = "%(mp4dash)s --verbose -f --use-segment-timeline %(dash_files)s -o %(output)s -m manifest.mpd "\
              "--use-segment-list --exec-dir %(exec_dir)s" % \
              ({
                   "output": output,
                   "mp4dash": MP4DASH,
                   "dash_files": " ".join(dashed_files),
                   "exec_dir": BENTO_EXEC
               })
    print dashcmd

    process = subprocess.Popen(dashcmd, shell=True, stdout=subprocess.PIPE)
    print process.communicate()
    return os.path.join(output,"manifest.mpd")

@task()
def package_hls(input):
    '''
    Packages a single bitrate as HLS
    Need to check if the file is from the filesystem or http
    '''
    logger = package_hls.get_logger()
    #logger.info(file)
    task_id = package_hls.request.id
    output_file = os.path.join(OUTPUT_DIR, "%(task_id)s_%(bitrate)s_%(count)s.ts" % ({
                                                                                      "task_id":task_id,
                                                                                      "bitrate":input["params"]["bitrate"],
                                                                                      "count":"%03d"
                                                                                      }))
    index_file = "index_%(task_id)s_%(bitrate)s.m3u8" % ({
                                                          "task_id":task_id,
                                                          "bitrate":input["params"]["bitrate"]
                                                          })
    cmd = ("%(ffmpeg)s -y -i %(filename)s -codec copy -map 0 -f segment -segment_list %(index_file)s" \
    " -segment_time %(segment_time)s -bsf h264_mp4toannexb -segment_list_type m3u8 %(output_file)s" \
     % {
        "filename":input["output_file"],
        "bitrate":input["params"]["bitrate"], 
        "count":"%03d",
        "index_file":index_file,
        "output_file":output_file,
        "segment_time":10,
        "ffmpeg":FFMPEG
    })
    #appendVariantPlaylist(bitrate)
    process = subprocess.Popen(cmd ,shell=True, stdout=subprocess.PIPE, stderr=subprocess.PIPE, cwd=OUTPUT_DIR)
    so, se = process.communicate()
    if process.returncode != 0:
        raise Exception
    
    logger.info(output_file)
    return {
                "task": "package_hls",
                "status":"done",
                "out":so,
                "err":se,
                "cmd":cmd,
                "returncode": process.returncode,
                "source_file": input["output_file"],
                "output_file": output_file,
                "index_file": index_file
                }

@task()
def create_hls_manifest(input):
    logger = create_hls_manifest.get_logger()
    task_id = create_hls_manifest.request.id
    logger.error("HLS Manifest: " + task_id)
    logger.error(input.results)
    twait = True

    # Dangerous, this has to go!
    # Will also currently wait until fragment_dash is done which is not what we want
    while twait:
        for item in input.results:
            logger.info(item.status)
            if item.status == "PENDING":
                twait = True
            else:
                twait=False

        #time.sleep(5)
    for item in input.results:
        if item.result["task"] == "package_hls":
            logger.info(item.result["index_file"])
    return input

@celery.task
def chordfinisher(input, *args, **kwargs ):
  return input

@task()
def dmap(it, callback):
    # Map a callback over an iterator and return as a group
    callback = subtask(callback)
    return group(callback.clone([arg,]) for arg in it)()
  
@task()
def stitch2(input):
    concat_string = ""
    task_id = stitch2.request.id
    for item in input:
        concat_string += item["output_url"] + "\|"
    # Drop the extra \| on the string
    concat_string = concat_string[:-2]  
    
    server_details = get_serverdetails()
    outfile = "%(task_id)s_concat.ts" % ({"task_id":task_id})
    
    output_file = os.path.join(OUTPUT_DIR, outfile)  
    output_url = "http://%(server)s:%(port)s/files/%(task_id)s_%(outfile)s" % ({
                                                                         "server":server_details["ip"],
                                                                         "port":server_details["socket"],
                                                                         "outfile":outfile,
                                                                         "task_id":task_id
                                                                         })
    
    cmd = "%(ffmpeg)s -y -i concat:%(concat_string)s -c copy %(output_file)s" %\
    ({
     "concat_string":concat_string,
     "ffmpeg":FFMPEG,
     "output_file":output_file
      })
    process = subprocess.Popen(cmd,shell=True, stdout=subprocess.PIPE, stderr=subprocess.PIPE)
    so, se = process.communicate()
    if process.returncode != 0:
        raise Exception
    # TODO raise an exception if this process fails
    return {
                "status":"done",
                "out":so,
                "err":se,
                "cmd":cmd,
                "returncode": process.returncode,
                "output_file":output_file,
                "output_url":output_url,
                "input":input
                }

@task()
def runcmd(id, cmd=None):
    # For some reason this fails withonly one parameter
    process = subprocess.Popen(cmd,shell=True, stdout=subprocess.PIPE, stderr=subprocess.PIPE)
    so, se = process.communicate()
    return {
                "status":"done",
                "out":so,
                "err":se,
                "cmd":cmd,
                "returncode": process.returncode
                }
 
 
@task()
def transform(source_path):
    #base, ext = os.path.splitext(source_path)
    #destination = "%s-transformed%s" % (base, ext)
    #print "Transforming data in %s to %s" % (source_path, destination)
    #shutil.copyfile(source_path, destination)
    return "OK"
 
 
@task()
def load(filepath):
    print "Loading data in %s and removing" % filepath
    os.remove(filepath)