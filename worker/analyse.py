'''
Script to perform analysis on video files with a focus on multi rate encode

'''

import re
import subprocess
import logging
import math
import json
import pygal
from datetime import datetime, timedelta

logger = logging.getLogger(__name__)

FFPROBE = "/usr/local/bin/ffprobe"
MP4INFO = "/Users/andrew/Desktop/workspace/hbbtv/Bento4-SDK-1-3-5-541.universal-apple-macosx/bin/Release/mp4info"

def get_mp4info(infile):
    '''

    :param infile:
    :return:

    Sample data structure
    {
"file":{
  "major_brand":"isom",
  "minor_version":512,
  "compatible_brands":["isom", "iso2", "avc1", "mp41"]
},
"movie":{
  "duration_ms":210400,
  "duration":210400,
  "time_scale":1000,
  "fragments":true
},
"tracks":[
{
  "flags":7,
  "flag_names":["ENABLED" ,"IN-MOVIE" ,"IN-PREVIEW"],
  "id":1,
  "type":"Video",
  "duration_ms":210400,
  "language":"und",
  "media":{
    "sample_count":0,
    "timescale":12800,
    "duration":0,
    "duration_ms":0,
    "bitrate":588.007
  },
  "display_width":512.000000,
  "display_height":288.000000,
  "sample_descriptions":[
{
"coding":"avc1",
"coding_name":"H.264",
"width":512,
"height":288,
"depth":24,
"avc_profile":77,
"avc_profile_name":"Main",
"avc_profile_compat":64,
"avc_level":21,
"avc_nalu_length_size":4,
"avc_sps": ["674d4015eca04012d808800000030080000019078b16cb"],
"avc_pps": ["68ebecb2"]
}  ]
},
{
  "flags":7,
  "flag_names":["ENABLED" ,"IN-MOVIE" ,"IN-PREVIEW"],
  "id":2,
  "type":"Audio",
  "duration_ms":210327,
  "language":"und",
  "media":{
    "sample_count":0,
    "timescale":44100,
    "duration":0,
    "duration_ms":0,
    "bitrate":64.000
  },
  "sample_descriptions":[
{
"coding":"mp4a",
"coding_name":"MPEG-4 Audio",
"stream_type":5,
"stream_type_name":"Audio",
"object_type":64,
"object_type_name":"MPEG-4 Audio",
"max_bitrate":720000,
"average_bitrate":64000,
"buffer_size":75000,
"decoder_info": "1210",
"mpeg_4_audio_object_type":2,
"mpeg_4_audio_object_type_name":"AAC Low Complexity",
"mpeg_4_audio_decoder_config":{
  "sampling_frequency":44100,
  "channels":2
},
"sample_rate":44100,
"sample_size":16,
"channels":2
}  ]
}]
}

    '''
    cmd = ("%(mp4info)s --show-layout --format json %(infile)s " %
           {
            "mp4info":MP4INFO,
            "infile":infile
            })
    process = subprocess.Popen(cmd, shell=True, stdout=subprocess.PIPE, stderr=subprocess.PIPE)
    so, se = process.communicate()
    result = json.loads(so)
    return json.dumps(result, indent=4)

def get_keyframes(infile, keyframe_int, fps):
    # Should be able to return when keyframes are expected which is every x seconds
    # this would mean we should have an iframe every x * fps
    # Specific value for ffprobe output
    # TODO move this to using -print_format json to get results as a json object
    lines_past_type = 20
    line_count = 0
    print_line = False
    frame = 0
    found_iframe = False
    output_str = ""
    next_keyframe = math.ceil(float(fps) * keyframe_int)
    key_framedist = next_keyframe

    cmd = FFPROBE + " -show_frames %s" % infile

    logger.info(cmd)
# -print_format json
    process = subprocess.Popen(cmd, shell=True, stdout=subprocess.PIPE, stderr=subprocess.PIPE)

    while True:
      line = process.stdout.readline()
      if line != '':
        #the real code does filtering here
        #print line.rstrip()
        if (line.rstrip()=="media_type=video"):
            print_line = True
        if (print_line and (line_count < lines_past_type)):

                #print line.rstrip()
            if re.match(r'pict_type=I.*',line):
                found_iframe = True
                #print line.rstrip()
            match = re.match(r'coded_picture_number=(.*)',line)
            if (match):
                frame =  match.group(1)
                if (found_iframe):
                    output_str += "%s," % frame
                    found_iframe = False
                    if (int(frame)>=next_keyframe):
                        if (int(frame)==next_keyframe):
                            print "Found I frame as expected at %s" % next_keyframe
                        else:
                            print "Expected I frame at %s found at %s" % (next_keyframe, frame)
                        next_keyframe+=key_framedist

            line_count+=1
            if (line_count >= lines_past_type):
                print_line = False
                line_count = 0
      else:
        break
    return output_str

def extract_bitrates(infile, outfile=None, graph="/tmp/graph.svg"):
    # Extracts bitrates used across all video frames

    cmd = "ffprobe -select_streams v -show_frames -print_format json " + infile
    process = subprocess.Popen(cmd, shell=True, stdout=subprocess.PIPE, stderr=subprocess.PIPE)
    so, se = process.communicate()

    results = json.loads(so)
    bitrates = []
    times = []
    pict_types = []
    if outfile is not None:
        f = open(outfile,'w')
    for item in results["frames"]:
        bitrates.append({"value":int(item["pkt_size"]), "label":item["pict_type"]})
        times.append(float(item["pkt_pts_time"]))
        #pict_types.append()
        if outfile is not None:
            f.write("%s, %s, %s\n" % (item["pkt_pts_time"], item["pkt_size"], item["pict_type"]))
        else:
            print item["pkt_pts_time"], item["pkt_size"], item["pict_type"]

    line_chart = pygal.Line(print_values=False)
    line_chart.title = 'Frame size'
    line_chart.x_labels = map(str, range(0, int(max(times))))
    line_chart.add('Bytes', bitrates)
    line_chart.add('Time', times)
    line_chart.render_to_file(graph)

def extract_keyframes(infile, outfile=None, graph="/tmp/graph.svg"):
    # Extracts bitrates used across all video frames

    cmd = "ffprobe -select_streams v -show_frames -print_format json -skip_frame nokey " + infile
    process = subprocess.Popen(cmd, shell=True, stdout=subprocess.PIPE, stderr=subprocess.PIPE)
    so, se = process.communicate()

    results = json.loads(so)
    bitrates = []
    times = []
    pict_types = []
    if outfile is not None:
        f = open(outfile,'w')


    for item in results["frames"]:
        #bitrates.append({"value":int(item["pkt_size"]), "label":item["pict_type"]})
        #times.append(float(item["pkt_pts_time"]))
        f = float(item["pkt_pts_time"])
        # convert secs to ms
        t = f * 1000
        ts = datetime.utcfromtimestamp(t)
        times.append((f, int(item["pkt_size"])))

        #pict_types.append()
        if outfile is not None:
            f.write("%s, %s, %s\n" % (item["pkt_pts_time"], item["pkt_size"], item["pict_type"]))
        else:
            print item["pkt_pts_time"], item["pkt_size"], item["pict_type"]

    from pygal.style import CleanStyle
    datey = pygal.XY(title=u'I Frame spacing and size', x_label_rotation=45, stroke=True, fill=True,
                     interpolate='cubic', style=CleanStyle, x_title='Time in seconds', print_values=True)
    datey.add("I", times)
    datey.render_to_file(graph)
