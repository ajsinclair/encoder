'''
Utility to join dash files together

'''

import os
import re
import fileinput
import subprocess

MP4DASHCLONE = "/Users/andrew/Desktop/workspace/hbbtv/Bento4-SDK-1-3-5-541.universal-apple-macosx/utils/mp4-dash-clone.py"

def parse_dash(url):
    '''
    This is what needs to be done to download a DASH file
    Get the Representations from the AdaptationSets
    <AdaptationSet mimeType="audio/mp4" segmentAlignment="true" startWithSAP="1" lang="und">
        <Representation bandwidth="494212" id="video.1" codecs="avc1.4d4015" frameRate="24000/1001" width="512" height="288">
            <SegmentTemplate duration="1960" initialization="3764143740001/video/1/init.mp4" media="3764143740001/video/1/seg-$Number$.m4f" startNumber="0" timescale="1000"/>

    From the initialization file we can get the duration using mp4info or mediainfo (supports url)
    From the segment template you can get the segment duration so using segment duration and total duration we can work out how
    many segments we should try and request
    e.g. duration / segment_duration
    Once we have the number of templates we can use the mediatemplate to create a list e.g.

    :param url:
    :return:
    '''

def download_dash(url, outputdir):
    cmd = "%(mp4dashclone)s --verbose %(url)s %(outputdir)s" % ({
            "mp4dashclone": MP4DASHCLONE,
            "url": url,
            "outputdir": outputdir
    })

    if not os.path.exists(outputdir):
        os.makedirs(outputdir)

    # Write a record of the URL used here
    urlfile = open(os.path.join(outputdir,"url.txt"),'w')
    urlfile.write(url)
    urlfile.close()

    process = subprocess.Popen(cmd, shell=True, stdout=subprocess.PIPE, cwd=outputdir)



    so, se = process.communicate()
    print se
    print so
    return outputdir

def get_num(txt):
    # Get the number from a dash pattern e.g. Seg-<num>.m4f
    r = re.search(r"seg-(\d+).m4f", txt)
    return int(r.group(1)) if r else '&'

def get_file_list(indir):
    files = os.listdir(indir)

    # Put the init.mp4 first and pop it from the sort
    files = sorted(files)
    init = files.pop(0)
    files.sort(key=lambda x: get_num(x))

    files.insert(0, init)

    # Now prepend the fullpath for further processing
    newlist = []
    for i in xrange(len(files)):
        files[i] = os.path.join(indir, files[i])
        print files[i]

    return files

def join_files(files, outfile):
    # Takes an ordered list of files and joins them together
    with open(outfile, 'w') as fout:
        for line in fileinput.input(files):
            fout.write(line)

    return outfile

def join(indir, outfile):
    '''
    Add an indirectory that contains the dash segments and the output files as an mp4
    :param indir:
    :param outfile:
    :return:
    '''
    return join_files(get_file_list(indir),outfile)



