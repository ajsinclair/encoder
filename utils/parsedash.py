__author__ = 'andrew'

import requests
import xml.dom.minidom

dashtest = "http://nine-hbb-poc.s3.amazonaws.com/manifests/MIMU103-1-C-High.mpd"

resp = requests.get(dashtest)

myxml = xml.dom.minidom.parseString(resp.text)

'''
 <AdaptationSet mimeType="audio/mp4" segmentAlignment="true" startWithSAP="1" lang="und">
        <Representation bandwidth="494212" id="video.1" codecs="avc1.4d4015" frameRate="24000/1001" width="512" height="288">
            <SegmentTemplate duration="1960" initialization="3764143740001/video/1/init.mp4" media="3764143740001/video/1/seg-$Number$.m4f" startNumber="0" timescale="1000"/>
'''

adaptation_sets = myxml.getElementsByTagName('AdaptationSet')

for set in adaptation_sets:
    print set.attributes["mimeType"].value
    rep = set.getElementsByTagName('Representation')
    for item in rep:
        try:
            print item.attributes["bandwidth"].value
            print item.attributes["id"].value

            print item.attributes["width"].value
            print item.attributes["height"].value
        except:
            pass
    settmpl = set.getElementsByTagName('SegmentTemplate')
    print rep, settmpl
